package com.netcracker.parfenenko.unit;

import com.netcracker.parfenenko.client.CustomerClient;
import com.netcracker.parfenenko.exception.DocumentNotFoundException;
import com.netcracker.parfenenko.model.Product;
import com.netcracker.parfenenko.repository.MongoTemplateProductRepository;
import com.netcracker.parfenenko.repository.SpringDataProductRepository;
import com.netcracker.parfenenko.service.ProductServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductApplicationUnitTest {

	@Mock
    private SpringDataProductRepository springDataProductRepository;
	@Mock
    private CustomerClient customerClient;
	@Mock
    private MongoTemplateProductRepository mongoTemplateProductRepository;
	@InjectMocks
    private ProductServiceImpl productService;
	
	@Before
    public void init() {
        MockitoAnnotations.initMocks(ProductApplicationUnitTest.class);
    }

    @Test
    public void saveTest() {
	    final String ID = "";
	    final String MEASURE = "kg";
        Product product = mock(Product.class);
        when(product.getCustomerId()).thenReturn(ID);
        when(customerClient.checkCustomerExistence(product.getCustomerId())).thenReturn(true);
        when(product.getMeasure()).thenReturn(MEASURE);
        when(springDataProductRepository.insert(Mockito.any(Product.class))).thenReturn(product);

        productService.save(product);
        InOrder inOrder = Mockito.inOrder(springDataProductRepository, customerClient);
        inOrder.verify(customerClient).checkCustomerExistence(ID);
        inOrder.verify(springDataProductRepository).insert(product);
    }

    @Test(expected = DocumentNotFoundException.class)
    public void saveWrongCustomerIdTest() {
        final String ID = "";
        Product product = mock(Product.class);
        when(product.getCustomerId()).thenReturn(ID);
        when(customerClient.checkCustomerExistence(product.getCustomerId())).thenReturn(false);

        productService.save(product);
    }

    @Test(expected = IllegalArgumentException.class)
    public void saveWrongMeasureTest() {
        final String id = "";
        String WRONG_MEASURE = "wrongMeasure";
        Product product = mock(Product.class);
        when(product.getCustomerId()).thenReturn(id);
        when(customerClient.checkCustomerExistence(product.getCustomerId())).thenReturn(true);
        when(product.getMeasure()).thenReturn(WRONG_MEASURE);

        productService.save(product);
    }

    @Test
    public void findByIdTest() {
        final String id = "";
        when(springDataProductRepository.findOne(Mockito.anyString())).thenReturn(Mockito.any(Product.class));

        productService.findById(id);
        verify(springDataProductRepository).findOne(id);
        verifyNoMoreInteractions(springDataProductRepository);
    }

    @Test
    public void findByNameTest() {
        final String name = "";
        when(springDataProductRepository.findByNameIgnoreCase(Mockito.anyString())).thenReturn(Mockito.anyListOf(Product.class));

        productService.findByName(name);
        verify(springDataProductRepository).findByNameIgnoreCase(name);
        verifyNoMoreInteractions(springDataProductRepository);
    }

    @Test
    public void findByFilterTest() {
	    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
        List<String> anyFilterList = new ArrayList<>(0);
	    Integer minValue = 0;
	    Integer maxValue = 1;

        Map<String, List<String>> filterMap = new HashMap<>();
        filterMap.put("customerId", anyFilterList);
        filterMap.put("shopName", anyFilterList);
        filterMap.put("manufacturer", anyFilterList);
        filterMap.put("measure", anyFilterList);
        filterMap.put("country", anyFilterList);
        //noinspection unchecked
        when(mongoTemplateProductRepository.findByFilter(
                anyMap(), Mockito.any(Integer.class), Mockito.any(Integer.class)))
                .thenReturn(new ArrayList<>());

        productService.findByFilter(filterMap, minValue, maxValue);
        verify(mongoTemplateProductRepository).findByFilter(filterMap, minValue, maxValue);
        verifyNoMoreInteractions(mongoTemplateProductRepository);
    }

    @SuppressWarnings("Duplicates")
    @Test(expected = IllegalArgumentException.class)
    public void findByWrongFilterTest() {
        @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
        List<String> anyFilterList = new ArrayList<>(0);
        Integer minValue = 0;
        Integer maxValue = 1;

        Map<String, List<String>> filterMap = new HashMap<>();
        filterMap.put("customerId", anyFilterList);
        filterMap.put("shopName", anyFilterList);
        filterMap.put("manufacturer", anyFilterList);
        filterMap.put("measure", anyFilterList);
        filterMap.put("country", anyFilterList);
        filterMap.put("redundantFilter", anyFilterList);

        productService.findByFilter(filterMap, minValue, maxValue);
    }

    @Test(expected = IllegalArgumentException.class)
    public void findByWrongQuantityValuesTest() {
        @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
        List<String> anyFilterList = new ArrayList<>(0);
        Integer minValue = 1;
        Integer maxValue = 0;

        Map<String, List<String>> filterMap = new HashMap<>();
        filterMap.put("customerId", anyFilterList);
        filterMap.put("shopName", anyFilterList);
        filterMap.put("manufacturer", anyFilterList);
        filterMap.put("measure", anyFilterList);
        filterMap.put("country", anyFilterList);

        productService.findByFilter(filterMap, minValue, maxValue);
    }

    @Test
    public void updateTest() {
        Product product = mock(Product.class);
        when(product.getId()).thenReturn("");
        when(springDataProductRepository.findOne(anyString())).thenReturn(product);
        when(springDataProductRepository.save(Mockito.any(Product.class))).thenReturn(product);

        productService.update(product);
        InOrder inOrder = Mockito.inOrder(springDataProductRepository, product);
        inOrder.verify(springDataProductRepository).findOne(product.getId());
        inOrder.verify(springDataProductRepository).save(product);
    }

    @Test(expected = DocumentNotFoundException.class)
    public void updateFailTest() {
        Product product = mock(Product.class);
        when(springDataProductRepository.findOne(anyString())).thenReturn(null);

        productService.update(product);
    }

    @Test
    public void deleteTest() {
        String id = "";
        when(springDataProductRepository.findOne(anyString())).thenReturn(new Product());
        doNothing().when(springDataProductRepository).delete(Mockito.anyString());

        productService.delete(id);
        InOrder inOrder = Mockito.inOrder(springDataProductRepository);
        inOrder.verify(springDataProductRepository).findOne(id);
        inOrder.verify(springDataProductRepository).delete(id);
    }

    @Test(expected = DocumentNotFoundException.class)
    public void deleteFailTest() {
        String id = "";
        when(springDataProductRepository.findOne(anyString())).thenReturn(null);

        productService.delete(id);
    }

    @Test
    public void deleteByCustomerIdTest() {
        String customerId = "";
        when(customerClient.checkCustomerExistence(anyString())).thenReturn(true);
        doNothing().when(springDataProductRepository).deleteByCustomerId(Mockito.anyString());

        productService.deleteByCustomerId(customerId);
        InOrder inOrder = Mockito.inOrder(springDataProductRepository, customerClient);
        inOrder.verify(customerClient).checkCustomerExistence(customerId);
        inOrder.verify(springDataProductRepository).deleteByCustomerId(customerId);
    }

    @Test(expected = DocumentNotFoundException.class)
    public void deleteByCustomerIdFailTest() {
        String customerId = "";
        when(customerClient.checkCustomerExistence(anyString())).thenReturn(false);

        productService.deleteByCustomerId(customerId);
    }

}
