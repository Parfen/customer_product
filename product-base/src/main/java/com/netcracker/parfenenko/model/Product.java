package com.netcracker.parfenenko.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Min;
import java.util.Objects;

@Document(collection = "products")
public class Product {

    @Id
    private String id;
    private String customerId;
    private String name;
    @Min(0)
    private int quantity;
    private String measure;
    private String country;
    private String manufacturer;
    private Shop shop;

    public Product() {
    }

    public Product(String id, String customerId, String name, int quantity, String measure, String country, String manufacturer, Shop shop) {
        this.id = id;
        this.customerId = customerId;
        this.name = name;
        this.quantity = quantity;
        this.measure = measure;
        this.country = country;
        this.manufacturer = manufacturer;
        this.shop = shop;
    }

    public String getId() {
        return id;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return quantity == product.quantity &&
                Objects.equals(id, product.id) &&
                Objects.equals(customerId, product.customerId) &&
                Objects.equals(name, product.name) &&
                Objects.equals(measure, product.measure) &&
                Objects.equals(country, product.country) &&
                Objects.equals(manufacturer, product.manufacturer) &&
                Objects.equals(shop, product.shop);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, customerId, name, quantity, measure, country, manufacturer, shop);
    }

    @Override
    public String toString() {
        return id;
    }

}
