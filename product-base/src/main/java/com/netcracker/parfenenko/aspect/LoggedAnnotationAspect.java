package com.netcracker.parfenenko.aspect;

import com.netcracker.parfenenko.annotation.Logged;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Aspect
@Component
@SuppressWarnings({"FieldCanBeLocal", "Duplicates"})
public class LoggedAnnotationAspect {

    private final String STARTED = "Operation of {} STARTED";
    private final String FINISHED = "Operation of {} FINISHED";

    @Around(value = "@annotation(logged)")
    public Object logMethodExecution(ProceedingJoinPoint joinPoint, Logged logged) throws Throwable {
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        Method method = methodSignature.getMethod();
        String operationName = getOperationName(logged.loggedArgs(), logged.operationName(), joinPoint.getArgs());
        final Logger LOGGER = LogManager.getLogger(method.getDeclaringClass());
        LOGGER.info(STARTED, operationName);
        Object returnedObject = joinPoint.proceed(joinPoint.getArgs());
        LOGGER.info(FINISHED, operationName);
        return returnedObject;
    }

    private String getOperationName(boolean loggedArgs, String operationName, Object[] args) {
        if (loggedArgs) {
            return String.format(operationName, args);
        } else {
            return operationName;
        }
    }

}
