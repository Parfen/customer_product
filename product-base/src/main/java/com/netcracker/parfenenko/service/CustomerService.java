package com.netcracker.parfenenko.service;

import com.netcracker.parfenenko.dto.CustomerDto;

import java.util.Map;

public interface CustomerService {

    CustomerDto[] findAll();

    CustomerDto[] findByNameFilter(Map<String, String> nameFilterMap);

    CustomerDto[] findByAddressFilter(Map<String, String> addressFilterMap);

    CustomerDto findByMail(String mail);

}
