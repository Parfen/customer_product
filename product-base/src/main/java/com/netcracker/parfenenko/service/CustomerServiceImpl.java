package com.netcracker.parfenenko.service;

import com.netcracker.parfenenko.annotation.Logged;
import com.netcracker.parfenenko.client.CustomerClient;
import com.netcracker.parfenenko.dto.CustomerDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@SuppressWarnings({"SpellCheckingInspection", "FieldCanBeLocal"})
@Service
public class CustomerServiceImpl implements CustomerService {

    private CustomerClient customerClient;

    private final String FIND_BY_NAME_FILTER = "searching for customers by name filters";
    private final String FIND_BY_ADDRESS_FILTER = "searching for customers by address filters";
    private final String FIND_BY_MAIL = "searching for customers by mail %s";
    private final String FIND_ALL = "searching for all customers";
    
    @Autowired
    public CustomerServiceImpl(CustomerClient customerClient) {
        this.customerClient = customerClient;
    }

    @Logged(operationName = FIND_ALL)
    public CustomerDto[] findAll() {
        return customerClient.findAll();
    }

    @Logged(operationName = FIND_BY_NAME_FILTER)
    public CustomerDto[] findByNameFilter(Map<String, String> nameFilterMap) {
        return customerClient.findByNameFilter(nameFilterMap);
    }

    @Logged(operationName = FIND_BY_ADDRESS_FILTER)
    public CustomerDto[] findByAddressFilter(Map<String, String> addressFilterMap) {
        return customerClient.findByAddressFilter(addressFilterMap);
    }

    @Logged(operationName = FIND_BY_MAIL, loggedArgs = true)
    public CustomerDto findByMail(String mail) {
        return customerClient.findByMail(mail);
    }

}
