package com.netcracker.parfenenko.service;

import com.netcracker.parfenenko.annotation.Logged;
import com.netcracker.parfenenko.client.CustomerClient;
import com.netcracker.parfenenko.exception.DocumentNotFoundException;
import com.netcracker.parfenenko.model.Product;
import com.netcracker.parfenenko.repository.MongoTemplateProductRepository;
import com.netcracker.parfenenko.repository.SpringDataProductRepository;
import com.netcracker.parfenenko.util.Measure;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SuppressWarnings("FieldCanBeLocal")
@Service
public class ProductServiceImpl implements ProductService {

    private SpringDataProductRepository springDataProductRepository;
    private CustomerClient customerClient;
    private final MongoTemplateProductRepository mongoTemplateProductRepository;

    private final String SAVE = "saving a product";
    private final String FIND_BY_ID = "searching for product with id %s";
    private final String FIND_BY_NAME = "searching for products with name %s";
    private final String FIND_BY_FILTER = "searching for products by filters";
    private final String UPDATE = "updating a product with id %s";
    private final String DELETE = "deleting a product with id %s";
    private final String DELETE_BY_CUSTOMER_ID = "deleting a product with customer id %s";

    @Autowired
    public ProductServiceImpl(SpringDataProductRepository springDataProductRepository, CustomerClient customerClient,
                              MongoTemplateProductRepository mongoTemplateProductRepository) {
        this.springDataProductRepository = springDataProductRepository;
        this.customerClient = customerClient;
        this.mongoTemplateProductRepository = mongoTemplateProductRepository;
    }

    @Logged(operationName = SAVE)
    public Product save(Product product) throws DocumentNotFoundException, IllegalArgumentException {
        if (!customerClient.checkCustomerExistence(product.getCustomerId())) {
            throw new DocumentNotFoundException("There is no customer with id " + product.getCustomerId());
        }
        measureValidation(product.getMeasure());
        return springDataProductRepository.insert(product);
    }

    @Logged(operationName = FIND_BY_ID, loggedArgs = true)
    public Product findById(String id) {
        return springDataProductRepository.findOne(id);
    }

    @Logged(operationName = FIND_BY_NAME, loggedArgs = true)
    public List<Product> findByName(String name) {
        return springDataProductRepository.findByNameIgnoreCase(name);
    }

    @Logged(operationName = FIND_BY_FILTER)
    public List<Product> findByFilter(Map<String, List<String>> filterMap, Integer minQuantity,
                                      Integer maxQuantity) throws IllegalArgumentException {
        if (!filterValidation(filterMap.keySet(), "customerId", "shopName", "manufacturer", "measure", "country")) {
            throw new IllegalArgumentException("Wrong filters. Filter map should contain some of the following keys: " +
                    "customerId, shopName, manufacturer, measure, country.");
        }
        measureValidation(filterMap.get("measure"));
        quantityValidation(minQuantity, maxQuantity);
        return mongoTemplateProductRepository.findByFilter(filterMap, minQuantity, maxQuantity);
    }

    @Logged(operationName = UPDATE, loggedArgs = true)
    public Product update(Product product) throws DocumentNotFoundException {
        Product existedProduct = springDataProductRepository.findOne(product.getId());
        if (existedProduct == null) {
            throw new DocumentNotFoundException("There is no product with id " + product.getId());
        }
        return springDataProductRepository.save(product);
    }

    @Logged(operationName = DELETE, loggedArgs = true)
    public void delete(String id) throws DocumentNotFoundException {
        if (springDataProductRepository.findOne(id) == null) {
            throw new DocumentNotFoundException("There is no product with id " + id);
        }
        springDataProductRepository.delete(id);
    }

    @Logged(operationName = DELETE_BY_CUSTOMER_ID, loggedArgs = true)
    public void deleteByCustomerId(String customerId) throws DocumentNotFoundException {
        if (!customerClient.checkCustomerExistence(customerId)) {
            throw new DocumentNotFoundException("There is no customer with id " + customerId);
        }
        springDataProductRepository.deleteByCustomerId(customerId);
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private boolean filterValidation(Set<String> filterKeySet, String... keys) {
        List<String> keyList = Arrays.asList(keys);
        for (String key : filterKeySet) {
            if (!keyList.contains(key)) {
                return false;
            }
        }
        return true;
    }

    private void measureValidation(List<String> measures) throws IllegalArgumentException {
        if (measures != null) {
            measures.forEach(this::measureValidation);
        }
    }

    private void measureValidation(String measure) throws IllegalArgumentException {
        if (!Measure.consists(measure)) {
            throw new IllegalArgumentException("Wrong measures type. Should be some of the following: kg, item, lt");
        }
    }

    private void quantityValidation(Integer minQuantity, Integer maxQuantity) throws IllegalArgumentException {
        if (minQuantity != null) {
            if (minQuantity < 0) {
                throw new IllegalArgumentException("Minimum quantity should not be less then 0");
            }
        }
        if (maxQuantity != null) {
            if (maxQuantity < 0) {
                throw new IllegalArgumentException("Maximum quantity should not be less then 0");
            }
        }
        if (minQuantity != null && maxQuantity != null) {
            if (maxQuantity < minQuantity) {
                throw new IllegalArgumentException("Maximum quantity should not be less then minimum quantity");
            }
        }
    }

}
