package com.netcracker.parfenenko.service;

import com.netcracker.parfenenko.exception.DocumentNotFoundException;
import com.netcracker.parfenenko.model.Product;

import java.util.List;
import java.util.Map;

public interface ProductService {

    Product save(Product product) throws DocumentNotFoundException, IllegalArgumentException;

    Product findById(String id);

    List<Product> findByName(String name);

    List<Product> findByFilter(Map<String, List<String>> filterMap, Integer minQuantity,
                               Integer maxQuantity) throws IllegalArgumentException;

    Product update(Product product) throws DocumentNotFoundException;

    void delete(String id) throws DocumentNotFoundException;

    void deleteByCustomerId(String customerId) throws DocumentNotFoundException;

}
