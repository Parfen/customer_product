package com.netcracker.parfenenko.client;

import com.netcracker.parfenenko.dto.CustomerDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Map;

@Component
@SuppressWarnings("FieldCanBeLocal")
public class CustomerClient {

    private RestTemplate restTemplate;

    @Value("${urn.customer-base}")
    private String urn;

    private final String CHECK_URL = "/existence";
    private final String FIND_ALL_URL = "";
    private final String FIND_BY_NAME_FILTER_URL = "/nameFilter";
    private final String FIND_BY_ADDRESS_FILTER_URL = "/addressFilter";
    private final String FIND_BY_MAIL_URL = "";

    @Autowired
    public CustomerClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean checkCustomerExistence(String customerId) {
        final String URI = getPath(urn, CHECK_URL);
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromUriString(URI)
                .queryParam("customerId", customerId);
        return restTemplate.getForObject(builder.toUriString(), Boolean.class);
    }

    public CustomerDto[] findAll() {
        return restTemplate.getForObject(getPath(urn, FIND_ALL_URL), CustomerDto[].class);
    }

    public CustomerDto[] findByNameFilter(Map<String, String> nameFilterMap) {
        return restTemplate.postForObject(getPath(urn, FIND_BY_NAME_FILTER_URL), nameFilterMap, CustomerDto[].class);
    }

    public CustomerDto[] findByAddressFilter(Map<String, String> addressFilterMap) {
        return restTemplate.postForObject(getPath(urn, FIND_BY_ADDRESS_FILTER_URL), addressFilterMap, CustomerDto[].class);
    }

    public CustomerDto findByMail(String mail) {
        final String URI = getPath(urn, FIND_BY_MAIL_URL);
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromUriString(URI)
                .queryParam("mail", mail);
        return restTemplate.getForObject(builder.toUriString(), CustomerDto.class);
    }

    private String getPath(String urn, String url) {
        String uri = "%s%s";
        return String.format(uri, urn, url);
    }

}
