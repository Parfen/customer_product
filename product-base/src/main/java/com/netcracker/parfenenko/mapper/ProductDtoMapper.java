package com.netcracker.parfenenko.mapper;

import com.netcracker.parfenenko.dto.ProductDto;
import com.netcracker.parfenenko.model.Product;
import org.springframework.stereotype.Component;

@Component
public class ProductDtoMapper extends GenericDtoMapper<Product, ProductDto> {

    public ProductDtoMapper() {
        super(Product.class, ProductDto.class);
    }

}
