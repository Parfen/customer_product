package com.netcracker.parfenenko.util;

public enum Measure {

    KILO("kg"),
    ITEM("item"),
    LITER("lt");

    private final String VALUE;

    Measure(String value) {
        this.VALUE = value;
    }

    public String value() {
        return VALUE;
    }

    public static boolean consists(String value) {
        return value.equals(KILO.VALUE) || value.equals(ITEM.VALUE) || value.equals(LITER.VALUE);
    }


}
