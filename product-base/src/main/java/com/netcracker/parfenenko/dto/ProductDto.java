package com.netcracker.parfenenko.dto;

import com.netcracker.parfenenko.model.Shop;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.Min;

@Data
@EqualsAndHashCode
public class ProductDto {

    private String customerId;
    private String name;
    @Min(0)
    private int quantity;
    private String measure;
    private String country;
    private String manufacturer;
    private Shop shop;

}
