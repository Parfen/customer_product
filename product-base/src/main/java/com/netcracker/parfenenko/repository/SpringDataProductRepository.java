package com.netcracker.parfenenko.repository;

import com.netcracker.parfenenko.model.Product;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SpringDataProductRepository extends MongoRepository<Product, String> {

    void deleteByCustomerId(String customerId);

    List<Product> findByNameIgnoreCase(String name);

}
