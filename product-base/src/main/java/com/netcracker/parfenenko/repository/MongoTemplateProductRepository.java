package com.netcracker.parfenenko.repository;

import com.netcracker.parfenenko.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class MongoTemplateProductRepository {

    private MongoTemplate mongoTemplate;

    @Autowired
    public MongoTemplateProductRepository(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public List<Product> findByFilter(Map<String, List<String>> filterMap, Integer minQuantity, Integer maxQuantity) {
        List<String> customerIds = filterMap.get("customerId");
        List<String> shopNames = filterMap.get("shopName");
        List<String> manufactures = filterMap.get("manufacture");
        List<String> measures = filterMap.get("measure");
        List<String> countries = filterMap.get("country");

        Criteria criteria = Criteria.where("_id").ne(0);
        criteria = addFilterQuery(criteria, customerIds, "customerId");
        criteria = addFilterQuery(criteria, shopNames, "shopName");
        criteria = addFilterQuery(criteria, manufactures, "manufacture");
        criteria = addFilterQuery(criteria, measures, "measure");
        criteria = addFilterQuery(criteria, countries, "country");
        criteria = addQuantityFilterQuery(criteria, minQuantity, maxQuantity);

        return mongoTemplate.find(new Query(criteria), Product.class);
    }

    private Criteria addFilterQuery(Criteria criteria, List<String> values, String fieldName) {
        if (values != null) {
            criteria = criteria.and(fieldName).in(values);
        }
        return criteria;
    }

    private Criteria addQuantityFilterQuery(Criteria criteria, Integer minQuantity, Integer maxQuantity) {
        if (minQuantity != null && maxQuantity != null) {
            criteria = criteria.and("quantity").gte(minQuantity).lte(maxQuantity);
        }
        else if (minQuantity != null) {
            criteria = criteria.and("quantity").gte(minQuantity);
        }
        else if (maxQuantity != null) {
            criteria = criteria.and("quantity").lte(maxQuantity);
        }
        return criteria;
    }

}
