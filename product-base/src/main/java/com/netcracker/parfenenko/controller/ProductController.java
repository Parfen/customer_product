package com.netcracker.parfenenko.controller;

import com.netcracker.parfenenko.dto.ProductDto;
import com.netcracker.parfenenko.mapper.DtoMapper;
import com.netcracker.parfenenko.mapper.ProductDtoMapper;
import com.netcracker.parfenenko.model.Product;
import com.netcracker.parfenenko.service.ProductService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/v1/product")
public class ProductController {

    private ProductService productService;
    private DtoMapper<Product, ProductDto> freshProductDtoMapper;

    @Autowired
    public ProductController(ProductService productService, ProductDtoMapper productDtoMapper) {
        this.productService = productService;
        this.freshProductDtoMapper = productDtoMapper;
    }

    @ApiOperation(httpMethod = "POST",
            value = "Saving a new product",
            response = Product.class,
            code = 201)
    @PostMapping
    public ResponseEntity<Product> save(@RequestBody @Valid ProductDto productDto) {
        Product product = freshProductDtoMapper.mapDto(productDto);
        return new ResponseEntity<>(productService.save(product), HttpStatus.CREATED);
    }

    @ApiOperation(httpMethod = "GET",
            value = "Searching for product by id",
            response = Product.class)
    @GetMapping(value = "/{id}")
    public ResponseEntity<Product> findById(@PathVariable String id) {
        return new ResponseEntity<>(productService.findById(id), HttpStatus.OK);
    }

    @ApiOperation(httpMethod = "GET",
            value = "Searching for products by name",
            response = Product.class,
            responseContainer = "List")
    @GetMapping(params = {"name"})
    public ResponseEntity<List<Product>> findByName(@RequestParam(name = "name") String name) {
        return new ResponseEntity<>(productService.findByName(name), HttpStatus.OK);
    }

    @ApiOperation(httpMethod = "POST",
            value = "Searching for products by filters",
            response = Product.class,
            responseContainer = "List")
    @PostMapping(value = "filteredProducts", params = {"minQuantity", "maxQuantity"})
    public ResponseEntity<List<Product>> findByAllFilter(@RequestBody Map<String, List<String>> filterMap,
                                                      @RequestParam(name = "minQuantity", required = false) Integer minQuantity,
                                                      @RequestParam(name = "maxQuantity", required = false) Integer maxQuantity) {
        return new ResponseEntity<>(productService.findByFilter(filterMap, minQuantity, maxQuantity), HttpStatus.OK);
    }

    @ApiOperation(httpMethod = "POST",
            value = "Searching for products by filters",
            response = Product.class,
            responseContainer = "List")
    @PostMapping(value = "filteredProducts", params = {"minQuantity"})
    public ResponseEntity<List<Product>> findByMinQuantityAndFilter(@RequestBody Map<String, List<String>> filterMap,
                                                      @RequestParam(name = "minQuantity", required = false) Integer minQuantity) {
        return new ResponseEntity<>(productService.findByFilter(filterMap, minQuantity, null), HttpStatus.OK);
    }

    @ApiOperation(httpMethod = "POST",
            value = "Searching for products by filters",
            response = Product.class,
            responseContainer = "List")
    @PostMapping(value = "filteredProducts", params = {"maxQuantity"})
    public ResponseEntity<List<Product>> findByMaxQuantityAndFilter(@RequestBody Map<String, List<String>> filterMap,
                                                      @RequestParam(name = "maxQuantity", required = false) Integer maxQuantity) {
        return new ResponseEntity<>(productService.findByFilter(filterMap, null, maxQuantity), HttpStatus.OK);
    }

    @ApiOperation(httpMethod = "POST",
            value = "Searching for products by filters",
            response = Product.class,
            responseContainer = "List")
    @PostMapping(value = "filteredProducts")
    public ResponseEntity<List<Product>> findByFilter(@RequestBody Map<String, List<String>> filterMap) {
        return new ResponseEntity<>(productService.findByFilter(filterMap, null, null), HttpStatus.OK);
    }

    @ApiOperation(httpMethod = "PUT",
            value = "Updating an existing product",
            response = Product.class)
    @PutMapping
    public ResponseEntity<Product> update(@RequestBody @Valid Product product) {
        return new ResponseEntity<>(productService.update(product), HttpStatus.OK);
    }

    @ApiOperation(httpMethod = "DELETE",
            value = "Deleting an existing product")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable String id) {
        productService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @ApiOperation(httpMethod = "DELETE",
            value = "Deleting products by customer ID")
    @DeleteMapping(params = {"customerId"})
    public ResponseEntity deleteByCustomerId(@RequestParam(name = "customerId") String customerId) {
        productService.deleteByCustomerId(customerId);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

}
