package com.netcracker.parfenenko.controller;

import com.netcracker.parfenenko.dto.CustomerDto;
import com.netcracker.parfenenko.service.CustomerService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(value = "/api/v1/customer")
public class CustomerController {

    private CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @ApiOperation(httpMethod = "GET",
            value = "Searching for all customers",
            response = CustomerDto[].class)
    @GetMapping
    public ResponseEntity<CustomerDto[]> findAll() {
        return new ResponseEntity<>(customerService.findAll(), HttpStatus.OK);
    }

    @ApiOperation(httpMethod = "POST",
            value = "Searching for customers by name filter",
            response = CustomerDto[].class)
    @PostMapping(value = "/nameFilter")
    public ResponseEntity<CustomerDto[]> findByNameFilter(@RequestBody Map<String, String> nameFilterMap) {
        return new ResponseEntity<>(customerService.findByNameFilter(nameFilterMap), HttpStatus.OK);
    }

    @ApiOperation(httpMethod = "POST",
            value = "Searching for customers by address filter",
            response = CustomerDto[].class)
    @PostMapping(value = "/addressFilter")
    public ResponseEntity<CustomerDto[]> findByAddressFilter(@RequestBody Map<String, String> addressFilterMap) {
        return new ResponseEntity<>(customerService.findByAddressFilter(addressFilterMap), HttpStatus.OK);
    }

    @ApiOperation(httpMethod = "GET",
            value = "Searching for customers by address filter",
            response = CustomerDto[].class)
    @GetMapping(params = {"mail"})
    public ResponseEntity<CustomerDto> findByMail(@RequestParam(name = "mail") @ApiParam(name = "mail") String mail) {
        return new ResponseEntity<>(customerService.findByMail(mail), HttpStatus.OK);
    }

}
