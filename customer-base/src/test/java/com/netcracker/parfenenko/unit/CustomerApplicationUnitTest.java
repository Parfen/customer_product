package com.netcracker.parfenenko.unit;

import com.netcracker.parfenenko.client.ProductClient;
import com.netcracker.parfenenko.repository.CustomerRepository;
import com.netcracker.parfenenko.service.CustomerServiceImpl;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerApplicationUnitTest {

    @Mock
    private CustomerRepository customerRepository;
    @Mock
    private ProductClient productClient;
    @InjectMocks
    private CustomerServiceImpl customerServiceImpl;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(CustomerApplicationUnitTest.class);
    }

	/*@Test
	public void saveTest() {
        Customer customer = mock(Customer.class);
        when(customerRepository.insert(Mockito.any(Customer.class))).thenReturn(customer);
        doNothing().when(customer).setDisplayName(Mockito.anyString());

        customerServiceImpl.save(customer);
        InOrder inOrder = Mockito.inOrder(customerRepository, customer);
        inOrder.verify(customerRepository).insert(customer);
        inOrder.verify(customer).setDisplayName(Mockito.anyString());
	}

	@Test
    public void findByIdTest() {
        String id = "";
        when(customerRepository.findOne(Mockito.anyString())).thenReturn(Mockito.any(Customer.class));

        customerServiceImpl.findById(id);
        verify(customerRepository).findOne(id);
        verifyNoMoreInteractions(customerRepository);
    }

    @Test
    public void findByNameFilterTest() {
        final String NAME = "nameValue";
        final String SURNAME = "surnameValue";
        final String PATRONYMIC = "patronymicValue";

        Map<String, String> nameFilterMap = new HashMap<>();
        nameFilterMap.put("name", NAME);
        nameFilterMap.put("surname", SURNAME);
        nameFilterMap.put("patronymic", PATRONYMIC);
        when(customerRepository.findByNameLikeIgnoreCaseAndSurnameLikeIgnoreCaseAndPatronymicLikeIgnoreCase(
                anyString(), anyString(), anyString()))
                .thenReturn(new ArrayList<>());

        customerServiceImpl.findByNameFilter(nameFilterMap);
        verify(customerRepository).findByNameLikeIgnoreCaseAndSurnameLikeIgnoreCaseAndPatronymicLikeIgnoreCase(
                NAME, SURNAME, PATRONYMIC);
        verifyNoMoreInteractions(customerRepository);
    }

    @Test(expected = IllegalArgumentException.class)
    public void findByWrongNameFilterTest() {
        final String NAME = "nameValue";
        final String SURNAME = "surnameValue";
        final String PATRONYMIC = "patronymicValue";
        final String REDUNDANT_FILTER = "anyFilterValue";

        Map<String, String> nameFilterMap = new HashMap<>();
        nameFilterMap.put("name", NAME);
        nameFilterMap.put("surname", SURNAME);
        nameFilterMap.put("patronymic", PATRONYMIC);
        nameFilterMap.put("redundantFilter", REDUNDANT_FILTER);

        customerServiceImpl.findByNameFilter(nameFilterMap);
    }

    @Test
    public void findByAddressFilterTest() {
        final String COUNTRY = "countryValue";
        final String CITY = "cityValue";
        final String STREET = "streetValue";
        final String HOUSE = "houseValue";

        Map<String, String> addressFilterMap = new HashMap<>();
        addressFilterMap.put("country", COUNTRY);
        addressFilterMap.put("city", CITY);
        addressFilterMap.put("street", STREET);
        addressFilterMap.put("house", HOUSE);
        when(customerRepository.findByAddressCountryLikeIgnoreCaseAndAddressCityLikeIgnoreCaseAndAddressStreetLikeIgnoreCaseAndAddressHouseLikeIgnoreCase(
                anyString(), anyString(), anyString(), anyString()))
                .thenReturn(new ArrayList<>());

        customerServiceImpl.findByAddressFilter(addressFilterMap);
        verify(customerRepository).findByAddressCountryLikeIgnoreCaseAndAddressCityLikeIgnoreCaseAndAddressStreetLikeIgnoreCaseAndAddressHouseLikeIgnoreCase(
                COUNTRY, CITY, STREET, HOUSE);
        verifyNoMoreInteractions(customerRepository);
    }

    @Test(expected = IllegalArgumentException.class)
    public void findByWrongAddressFilterTest() {
        final String COUNTRY = "countryValue";
        final String CITY = "cityValue";
        final String STREET = "streetValue";
        final String HOUSE = "houseValue";

        Map<String, String> addressFilterMap = new HashMap<>();
        addressFilterMap.put("country", COUNTRY);
        addressFilterMap.put("city", CITY);
        addressFilterMap.put("street", STREET);
        //noinspection SpellCheckingInspection
        addressFilterMap.put("hous", HOUSE);

        customerServiceImpl.findByNameFilter(addressFilterMap);
    }

    @Test
    public void findByMailFilter() {
        String mail = "";
        when(customerRepository.findByMail(Mockito.anyString())).thenReturn(Mockito.any(Customer.class));

        customerServiceImpl.findByMail(mail);
        verify(customerRepository).findByMail(mail);
        verifyNoMoreInteractions(customerRepository);
    }

    @Test
    public void findAllTest() {
        when(customerRepository.findAll()).thenReturn(new ArrayList<>());

        customerServiceImpl.findAll();
        verify(customerRepository).findAll();
        verifyNoMoreInteractions(customerRepository);
    }

    @Test
    public void updateTest() {
        Customer customer = mock(Customer.class);
        when(customer.getId()).thenReturn("");
        when(customerRepository.findOne(anyString())).thenReturn(customer);
        when(customerRepository.save(Mockito.any(Customer.class))).thenReturn(customer);
        doNothing().when(customer).setDisplayName(Mockito.anyString());

        customerServiceImpl.update(customer);
        InOrder inOrder = Mockito.inOrder(customerRepository, customer);
        inOrder.verify(customerRepository).findOne(customer.getId());
        inOrder.verify(customer).setDisplayName(Mockito.anyString());
        inOrder.verify(customerRepository).save(customer);
    }

    @Test(expected = DocumentNotFoundException.class)
    public void updateFailTest() {
        Customer customer = mock(Customer.class);
        when(customerRepository.findOne(anyString())).thenReturn(null);

        customerServiceImpl.update(customer);
    }

    @Test
    public void deleteTest() {
        String id = "";
        when(customerRepository.findOne(anyString())).thenReturn(new Customer());
        doNothing().when(customerRepository).delete(Mockito.anyString());
        doNothing().when(productClient).deleteByCustomerId(Mockito.anyString());

        customerServiceImpl.delete(id);
        InOrder inOrder = Mockito.inOrder(customerRepository, productClient);
        inOrder.verify(customerRepository).findOne(id);
        inOrder.verify(productClient).deleteByCustomerId(id);
        inOrder.verify(customerRepository).delete(id);
    }

    @Test(expected = DocumentNotFoundException.class)
    public void deleteFailTest() {
        String id = "";
        when(customerRepository.findOne(anyString())).thenReturn(null);

        customerServiceImpl.delete(id);
    }
*/
}
