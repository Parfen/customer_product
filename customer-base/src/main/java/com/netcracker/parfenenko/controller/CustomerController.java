package com.netcracker.parfenenko.controller;

import com.netcracker.parfenenko.dto.FreshCustomerDto;
import com.netcracker.parfenenko.dto.UpdateCustomerDto;
import com.netcracker.parfenenko.mapper.DtoMapper;
import com.netcracker.parfenenko.mapper.FreshCustomerDtoMapper;
import com.netcracker.parfenenko.mapper.UpdateCustomerDtoMapper;
import com.netcracker.parfenenko.model.Customer;
import com.netcracker.parfenenko.service.CustomerService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/v1/customer")
public class CustomerController {

    private CustomerService customerService;
    private DtoMapper<Customer, FreshCustomerDto> freshCustomerDtoMapper;
    private DtoMapper<Customer, UpdateCustomerDto> updateCustomerDtoMapper;

    @Autowired
    public CustomerController(CustomerService customerService, FreshCustomerDtoMapper freshCustomerDtoMapper,
                              UpdateCustomerDtoMapper updateCustomerDtoMapper) {
        this.customerService = customerService;
        this.freshCustomerDtoMapper = freshCustomerDtoMapper;
        this.updateCustomerDtoMapper = updateCustomerDtoMapper;
    }

    @ApiOperation(httpMethod = "POST",
            value = "Saving a new customer",
            response = Customer.class,
            code = 201)
    @PostMapping
    public ResponseEntity<Mono<Customer>> save(@RequestBody @Valid FreshCustomerDto freshCustomerDto) {
        return new ResponseEntity<>(customerService.save(freshCustomerDtoMapper.mapDto(freshCustomerDto)), HttpStatus.CREATED);
    }

    @ApiOperation(httpMethod = "GET",
            value = "Searching for customer by id",
            response = Customer.class)
    @GetMapping(value = "/{id}")
    public ResponseEntity<Mono<Customer>> findById(@PathVariable String id) {
        return new ResponseEntity<>(customerService.findById(id), HttpStatus.OK);
    }

    @ApiOperation(httpMethod = "GET",
            value = "Searching for all customers",
            response = Customer.class)
    @GetMapping
    public Flux<Customer> findAll() {
        return customerService.findAll();
    }

    @ApiOperation(httpMethod = "POST",
            value = "Searching for customers by name filter",
            response = Customer.class,
            responseContainer = "List")
    @PostMapping(value = "/nameFilter")
    public ResponseEntity<Flux<Customer>> findByNameFilter(@RequestBody Map<String, String> nameFilterMap) {
        return new ResponseEntity<>(customerService.findByNameFilter(nameFilterMap), HttpStatus.OK);
    }

    @ApiOperation(httpMethod = "POST",
            value = "Searching for customers by address filter",
            response = Customer.class,
            responseContainer = "List")
    @PostMapping(value = "/addressFilter")
    public ResponseEntity<Flux<Customer>> findByAddressFilter(@RequestBody Map<String, String> addressFilterMap) {
        return new ResponseEntity<>(customerService.findByAddressFilter(addressFilterMap), HttpStatus.OK);
    }

    @ApiOperation(httpMethod = "GET",
            value = "Searching for customer by mail",
            response = Customer.class)
    @GetMapping(params = {"mail"})
    public ResponseEntity<Mono<Customer>> findByMail(@RequestParam(name = "mail") @ApiParam(name = "mail") String mail) {
        return new ResponseEntity<>(customerService.findByMail(mail), HttpStatus.OK);
    }

    @ApiOperation(httpMethod = "PUT",
            value = "Updating an existing customer",
            response = Customer.class)
    @PutMapping
    public ResponseEntity<Mono<Customer>> update(@RequestBody @Valid UpdateCustomerDto updateCustomerDto) {
        Customer customer = updateCustomerDtoMapper.mapDto(updateCustomerDto);
        return new ResponseEntity<>(customerService.update(customer), HttpStatus.OK);
    }

    @ApiOperation(httpMethod = "DELETE",
            value = "Deleting an existing customer",
            response = Customer.class)
    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable String id) {
        customerService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @ApiOperation(httpMethod = "GET",
            value = "Check customer existence",
            response = Boolean.class)
    @GetMapping(value = "/existence", params = {"customerId"})
    public ResponseEntity<Mono<Boolean>> checkCustomerExistence(@RequestParam(name = "customerId") String id) {
        return new ResponseEntity<>(customerService.checkCustomerExistence(id), HttpStatus.OK);
    }
}
