package com.netcracker.parfenenko.mapper;

import com.netcracker.parfenenko.dto.FreshCustomerDto;
import com.netcracker.parfenenko.model.Customer;
import org.springframework.stereotype.Component;

@Component
public class FreshCustomerDtoMapper extends GenericDtoMapper<Customer, FreshCustomerDto> {

    public FreshCustomerDtoMapper() {
        super(Customer.class, FreshCustomerDto.class);
    }

}
