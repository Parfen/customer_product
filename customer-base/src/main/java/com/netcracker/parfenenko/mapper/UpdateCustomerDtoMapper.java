package com.netcracker.parfenenko.mapper;

import com.netcracker.parfenenko.dto.UpdateCustomerDto;
import com.netcracker.parfenenko.model.Customer;
import org.springframework.stereotype.Component;

@Component
public class UpdateCustomerDtoMapper extends GenericDtoMapper<Customer, UpdateCustomerDto> {

    public UpdateCustomerDtoMapper() {
        super(Customer.class, UpdateCustomerDto.class);
    }

}
