package com.netcracker.parfenenko.service;

import com.netcracker.parfenenko.exception.DocumentNotFoundException;
import com.netcracker.parfenenko.model.Customer;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;

public interface CustomerService {

    Mono<Customer> save(Customer customer);

    Mono<Customer> findById(String id);

    Flux<Customer> findByNameFilter(Map<String, String> nameFilter) throws IllegalArgumentException;

    Flux<Customer> findByAddressFilter(Map<String, String> addressFilterMap) throws IllegalArgumentException;

    Mono<Customer> findByMail(String mail);

    Flux<Customer> findAll();

    Mono<Customer> update(Customer customer) throws DocumentNotFoundException;

    void delete(String id) throws DocumentNotFoundException;

    Mono<Boolean> checkCustomerExistence(String id);

}
