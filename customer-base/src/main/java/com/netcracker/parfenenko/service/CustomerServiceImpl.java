package com.netcracker.parfenenko.service;

import com.netcracker.parfenenko.annotation.Logged;
import com.netcracker.parfenenko.client.ProductClient;
import com.netcracker.parfenenko.exception.DocumentNotFoundException;
import com.netcracker.parfenenko.model.Customer;
import com.netcracker.parfenenko.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
@SuppressWarnings("FieldCanBeLocal")
public class CustomerServiceImpl implements CustomerService {

    private CustomerRepository customerRepository;
    private ProductClient productClient;

    private final String SAVE = "saving a customer";
    private final String FIND_BY_ID = "searching for customer with id %s";
    private final String FIND_BY_NAME_FILTER = "searching for customers by name filters";
    private final String FIND_BY_ADDRESS_FILTER = "searching for customers by address filters";
    private final String FIND_BY_MAIL = "searching for customers by mail %s";
    private final String FIND_ALL = "searching for all customers";
    private final String UPDATE = "updating a customer with id %s";
    private final String DELETE = "deleting a customer with id %s";
    private final String CHECK_EXISTENCE = "checking existence of customer with id %s";

    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository, ProductClient productClient) {
        this.customerRepository = customerRepository;
        this.productClient = productClient;
    }

    @Logged(operationName = SAVE)
    public Mono<Customer> save(Customer customer) {
        return customerRepository
                .insert(customer)
                .doOnSuccess(customerParam -> {
                    customerParam.setDisplayName("Customer " + "#" + customerParam.getId());
                    customerRepository.save(customerParam).subscribe();
                });
    }

    @Logged(operationName = FIND_BY_ID, loggedArgs = true)
    public Mono<Customer> findById(String id) {
        return customerRepository.findById(id);
    }

    @Logged(operationName = FIND_BY_NAME_FILTER)
    public Flux<Customer> findByNameFilter(Map<String, String> nameFilter) throws IllegalArgumentException {
        if (!filterValidation(nameFilter.keySet(), "name", "surname", "patronymic")) {
            throw new IllegalArgumentException("Wrong filters. Filter map should contain some of the following keys: " +
                    "name, surname, patronymic.");
        }
        String name = getRealFilter(nameFilter, "name");
        String surname = getRealFilter(nameFilter, "surname");
        String patronymic = getRealFilter(nameFilter, "patronymic");
        return customerRepository.findByNameLikeIgnoreCaseAndSurnameLikeIgnoreCaseAndPatronymicLikeIgnoreCase(
                name, surname, patronymic);
    }

    @Logged(operationName = FIND_BY_ADDRESS_FILTER)
    public Flux<Customer> findByAddressFilter(Map<String, String> addressFilterMap) throws IllegalArgumentException {
        if (!filterValidation(addressFilterMap.keySet(), "country", "city", "street", "house")) {
            throw new IllegalArgumentException("Wrong filters. Filter map should contain some of the following keys: " +
                    "country, city, street, house.");
        }
        String country = getRealFilter(addressFilterMap, "country");
        String city = getRealFilter(addressFilterMap, "city");
        String street = getRealFilter(addressFilterMap, "street");
        String house = getRealFilter(addressFilterMap, "house");
        return customerRepository.findByAddressCountryLikeIgnoreCaseAndAddressCityLikeIgnoreCaseAndAddressStreetLikeIgnoreCaseAndAddressHouseLikeIgnoreCase(
                country, city, street, house);
    }

    @Logged(operationName = FIND_BY_MAIL, loggedArgs = true)
    public Mono<Customer> findByMail(String mail) {
        return customerRepository.findByMail(mail);
    }

    @Logged(operationName = FIND_ALL)
    public Flux<Customer> findAll() {
        return customerRepository.findAll();
    }

    @Logged(operationName = UPDATE, loggedArgs = true)
    public Mono<Customer> update(Customer customer) throws DocumentNotFoundException {
        return customerRepository
                .findById(customer.getId())
                .doOnSuccess(customerParam -> {
                    if (customerParam == null) {
                        throw new DocumentNotFoundException("There is no customer with id " + customer.getId());
                    }
                    customer.setDisplayName(customerParam.getDisplayName());
                    customerRepository.save(customer).subscribe();
                }).map(customerParam -> customer);
    }

    @Logged(operationName = DELETE, loggedArgs = true)
    public void delete(String id) throws DocumentNotFoundException {
        customerRepository
                .findById(id)
                .doOnSuccess(customerParam -> {
                    if (customerParam == null) {
                        throw new DocumentNotFoundException("There is no customer with id " + id);
                    }
                    productClient.deleteByCustomerId(id);
                    customerRepository.deleteById(id).subscribe();
                }).subscribe();
    }

    @Logged(operationName = CHECK_EXISTENCE, loggedArgs = true)
    public Mono<Boolean> checkCustomerExistence(String id) {
        return customerRepository.findById(id).hasElement();
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private boolean filterValidation(Set<String> filterKeySet, String... keys) {
        List<String> keyList = Arrays.asList(keys);
        for (String key : filterKeySet) {
            if (!keyList.contains(key)) {
                return false;
            }
        }
        return true;
    }

    private String getRealFilter(Map<String, String> filterMap, String filterName) {
        String filter = filterMap.get(filterName);
        if (filter == null) {
            filter = "";
        }
        return filter;
    }
}
