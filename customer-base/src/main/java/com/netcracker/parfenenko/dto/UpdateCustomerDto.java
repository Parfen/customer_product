package com.netcracker.parfenenko.dto;

import com.netcracker.parfenenko.model.Address;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Email;

@Data
@EqualsAndHashCode
public class UpdateCustomerDto {

    private String id;
    private String name;
    private String surname;
    private String patronymic;
    private Address address;
    @Email
    private String mail;

}
