package com.netcracker.parfenenko.repository;

import com.netcracker.parfenenko.model.Customer;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface CustomerRepository extends ReactiveMongoRepository<Customer, String> {

    Mono<Customer> findById(String id);

    Flux<Customer> findByNameLikeIgnoreCaseAndSurnameLikeIgnoreCaseAndPatronymicLikeIgnoreCase(
            String name, String surname, String patronymic);

    Mono<Customer> findByMail(String mail);

    Flux<Customer> findByAddressCountryLikeIgnoreCaseAndAddressCityLikeIgnoreCaseAndAddressStreetLikeIgnoreCaseAndAddressHouseLikeIgnoreCase(
            String country, String city, String street, String house);

}
