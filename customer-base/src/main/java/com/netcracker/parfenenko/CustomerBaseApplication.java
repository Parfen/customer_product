package com.netcracker.parfenenko;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerBaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerBaseApplication.class, args);
	}
}
