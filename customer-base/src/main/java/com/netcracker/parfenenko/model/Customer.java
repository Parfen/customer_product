package com.netcracker.parfenenko.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document(collection = "customers")
public class Customer {

    @Id
    private String id;
    private String name;
    private String surname;
    private String patronymic;
    private String displayName;
    private Address address;
    private String mail;

    public Customer() {
    }

    public Customer(String name, String surname, String patronymic, Address address, String mail) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.address = address;
        this.mail = mail;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return Objects.equals(id, customer.id) &&
                Objects.equals(name, customer.name) &&
                Objects.equals(surname, customer.surname) &&
                Objects.equals(patronymic, customer.patronymic) &&
                Objects.equals(displayName, customer.displayName) &&
                Objects.equals(address, customer.address) &&
                Objects.equals(mail, customer.mail);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname, patronymic, displayName, address, mail);
    }

    @Override
    public String toString() {
        return id;
    }

}
