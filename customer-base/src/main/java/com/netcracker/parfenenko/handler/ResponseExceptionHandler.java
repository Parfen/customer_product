package com.netcracker.parfenenko.handler;

import com.netcracker.parfenenko.exception.DocumentNotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ResponseExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOGGER = LogManager.getLogger(ResponseExceptionHandler.class);

    @ExceptionHandler(value = {DocumentNotFoundException.class})
    public ResponseEntity<Object> handleNotFoundConflict(DocumentNotFoundException exception, WebRequest request) {
        logErrorMessage(exception);
        return buildResponseEntity(exception, HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(value = {IllegalArgumentException.class})
    public ResponseEntity<Object> handleBadRequestConflict(IllegalArgumentException exception, WebRequest request) {
        logErrorMessage(exception);
        return buildResponseEntity(exception, HttpStatus.BAD_REQUEST, request);
    }

    private ResponseEntity<Object> buildResponseEntity(Exception exception, HttpStatus httpStatus, WebRequest request) {
        return handleExceptionInternal(exception, exception.getMessage(), new HttpHeaders(), httpStatus, request);
    }

    private void logErrorMessage(Exception e) {
        LOGGER.error(String.format("ERROR: %s. Stack trace: ", e.getMessage()), e);
    }

}
