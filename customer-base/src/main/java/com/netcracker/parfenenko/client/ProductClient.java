package com.netcracker.parfenenko.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class ProductClient {

    private RestTemplate restTemplate;

    @Value("${urn.product-base}")
    private String urn;
    @SuppressWarnings("FieldCanBeLocal")
    private final String DELETE_BY_CUSTOMER_ID_URL = "";

    @Autowired
    public ProductClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public void deleteByCustomerId(String customerId) {
        final String URI = getPath(urn, DELETE_BY_CUSTOMER_ID_URL);
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromUriString(URI)
                .queryParam("customerId", customerId);
        restTemplate.delete(builder.toUriString());
    }

    private String getPath(String urn, String url) {
        String uri = "%s%s";
        return String.format(uri, urn, url);
    }

}
