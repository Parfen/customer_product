package com.netcracker.parfenenko.exception;

public class DocumentNotFoundException extends RuntimeException {

    public DocumentNotFoundException(String message) {
        super(message);
    }

    public DocumentNotFoundException() {
        super("Document not found");
    }

}
